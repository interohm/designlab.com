---
name: Color
---

## Primary colors

Charcoal and White are the base colors used in all branded materials. The remaining oranges and purples in the primary palette are used in reinforcing elements, such as graphics and illustrations.

[Color swatches](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-product-marketing/brand-design/-/tree/e6e2bb24e899078935d1aeb0e65c226b6bf36a8b/brand/brand-assets/brand-color-palettes) are denoted with a _p_ (for primary palette), _g_ (for gradients), or _s_ (for secondary palette). Refer to the [color usage](#color-usage) section for more information on when to use digital vs. print colors.

### Primary color palette

| **Swatch**                                                                                                          | **HEX** | **RGB**       | **CMYK**       | **PMS**                        |
| ------------------------------------------------------------------------------------------------------------------- | ------- | ------------- | -------------- | ------------------------------ |
| <div class="gl-p-3" style="background-color:#ffffff;"><span class="variable">White</span> </div>                    | #FFFFFF | 255, 255, 255 | 0, 0, 0, 0     | White                          |
| <div class="gl-p-3 gl-text-white" style="background-color:#171321;"><span class="variable">Charcoal</span> </div>   | #171321 | 23, 19, 33    | 90, 68, 41, 90 | 433 C / 4280 U                 |
| <div class="gl-p-3" style="background-color:#FCA326;"><span class="variable">Orange 01p</span> </div>               | #FCA326 | 252, 161, 33  | 0, 40, 80, 5   | 143 C / 1365 U                 |
| <div class="gl-p-3" style="background-color:#FC6D26;"><span class="variable">Orange 02p</span> </div>               | #FC6D26 | 252, 109, 38  | 0, 60, 80, 5   | 716 C / 144 U                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#E24329;"><span class="variable">Orange 03p</span> </div> | #E24329 | 226, 67, 41   | 0, 75, 80, 10  | 7417 C / 1665 U                |
| <div class="gl-p-3" style="background-color:#A989F5;"><span class="variable">Purple 01p</span> </div>               | #A989F5 | 169, 137, 245 | 53, 55, 0, 0   | 2655 C / 2645 CU               |
| <div class="gl-p-3 gl-text-white" style="background-color:#7759C2;"><span class="variable">Purple 02p</span> </div> | #7759C2 | 119, 89, 194  | 64, 70, 0, 0   | 2665 C / 2593 U                |

## Secondary colors

The secondary palette expands upon our oranges and purples to provide more color varieties. These additional hues can be used to highlight various elements and add visual depth where needed. This palette also introduces teal, which has a positive connotation, especially when used to reference product features. The cool grays offer neutral color options to be used for differentiating information and/or establishing hierarchy.

### Secondary color palette

| **Swatch**                                                                                                          | **HEX** | **RGB**       | **CMYK**       | **PMS**                        |
| ------------------------------------------------------------------------------------------------------------------- | ------- | ------------- | -------------- | ------------------------------ |
| <div class="gl-p-3" style="background-color:#CEB3EF;"><span class="variable">Purple 01s</span> </div>                  | #CEB3EF | 206, 179, 239 | 24, 29, 0, 0    | 2635 C |
| <div class="gl-p-3 gl-text-white" style="background-color:#5943B6;"><span class="variable">Purple 02s</span> </div>                  | #5943B6 | 89, 67, 182 | 77, 82, 0, 0 | 2097 C                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#2F2A6B;"><span class="variable">Purple 03s</span> </div>    | #2F2A6B | 47, 42, 107 | 98, 98, 27, 15 | 273 C                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#232150;"><span class="variable">Purple 04s</span> </div>    | #232150 | 35, 33, 80    | 85, 82, 42, 38 | 275 C                 |
| <div class="gl-p-3" style="background-color:#FDF1DD;"><span class="variable">Orange 01s</span> </div>    | #FDF1DD | 253, 241, 221    | 1, 4, 12, 0 | 7506 C             |
| <div class="gl-p-3" style="background-color:#C5F4EC;"><span class="variable">Teal 01s</span> </div>    | #C5F4EC | 197, 244, 236    | 20, 0, 11, 0 | 317 C             |
| <div class="gl-p-3" style="background-color:#6FDAC9;"><span class="variable">Teal 02s</span> </div>                  | #6FDAC9 | 111, 218, 201 | 51, 0, 29, 0    | 3242 C |
| <div class="gl-p-3" style="background-color:#10B1B1;"><span class="variable">Teal 03s</span> </div>                  | #10B1B1 | 16, 177, 177 | 75, 5, 35, 0 | 7710 C                  |
| <div class="gl-p-3" style="background-color:#D1D0D3;"><span class="variable">Gray 01</span> </div>                  | #D1D0D3 | 209, 208, 211 | 8, 5, 7, 16    | Cool Gray 3 C /  Cool Gray 2 U |
| <div class="gl-p-3" style="background-color:#A2A1A6;"><span class="variable">Gray 02</span> </div>                  | #A2A1A6 | 162, 161, 166 | 19, 12, 13, 34 | 422 C / 422 U                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#74717A;"><span class="variable">Gray 03</span> </div>    | #74717A | 116, 113, 122 | 30, 20, 19, 58 | 424 C / 425 U                  |
| <div class="gl-p-3 gl-text-white" style="background-color:#45424D;"><span class="variable">Gray 04</span> </div>    | #45424D | 69, 66, 77    | 41, 28, 22, 70 | 7540 C / 419 U                 |
| <div class="gl-p-3 gl-text-white" style="background-color:#2B2838;"><span class="variable">Gray 05</span> </div>    | #2B2838 | 43, 40, 56    | 94, 77, 53, 94 | 426 C / Black 6 U              |

## Gradients

Gradients should not be overused and should be leveraged as a key element to add additional interest, highlight, or depth. It can be used in more subtle ways, like minimal highlighting or adding shadows, or it can be used in large-scale ways when referring to the product as a whole.

To create the gradient, use the Adobe Illustrator free-form gradient tool found in the gradient panel. Smooth, natural blending is accomplished by using a graduated blend of color stops within a shape in an ordered or random sequence.

Apply the gradient using points. Use between 3-5 colors. To achieve softer colors, use the HSB palette and drop saturation of any brand color between 1-50%. The gradient sections can also be cropped to achieve a softer look.

### Gradient color palette

| **Swatch**                                                                                            | **HEX** | **RGB**       | **CMYK**     | **PMS**         |
| ----------------------------------------------------------------------------------------------------- | ------- | ------------- | ------------ | --------------- |
| <div class="gl-p-3" style="background-color:#FFD1BF;"><span class="variable">Orange 01g</span> </div> | #FFD1BF | 255, 209, 191 | 0, 20, 21, 0 | 489 C / 489 U   |
| <div class="gl-p-3" style="background-color:#CEB3EF;"><span class="variable">Purple 01g</span> </div> | #CEB3EF | 206, 179, 239 | 24, 29, 0, 0 | 2635 C / 2635 U |
| <div class="gl-p-3" style="background-color:#FFB9C9;"><span class="variable">Pink 01g</span> </div>   | #FFB9C9 | 255, 185, 201 | 0, 33, 10, 0 | 1767 C / 1767 U |

## Contrast and accessibility

Using color combinations that have sufficient contrast benefits everyone and preserves readability. A contrast ratio of 4.5:1 or higher is preferred for text and UI elements, but 3:1 can be used for larger text. Illustrations are an exception and will use a larger range of the palette, although even the key elements should be clearly defined.

Color combinations (foreground and background) shown here have sufficient contrast. Charcoal is the only color that should be used on gradients to ensure sufficient contrast over every part of the gradient.

Color accessibility enables people with visual impairments or color vision deficiencies to interact with digital experiences in the same way as their non-visually-impaired counterparts. For more information about color contrast in a digital context, view the [WCAG Success Criterion 1.4.3 Contrast (Minimum)](https://www.w3.org/TR/WCAG21/#contrast-minimum) guidelines.

## Color usage

### Digital

**Digital colors** refer to colors that will be used for digital properties. For example, this webpage uses color defined by HEX codes. If you're working with any digital asset, you can input either the HEX or RGB color codes, as detailed in the primary palette. Some digital use cases include, digital ads, digital billboards, video and animation, Canva.

- HEX: HEX codes are based on the hexadecimal system in computing. HEX and RGB codes provide the same information, but in different formats. The HEX code includes a hashtag followed by six characters.
  - **Example:** `#FC6D26`
- RGB: RGB is an acronym for Red, Green, and Blue. All digital colors are generated using percentages of these three colors. As mentioned, RGB is interchangeable with HEX.
  - **Example:** `rgb(255, 255, 255)`

### Print

**Print colors** refer to colors that will be used for printed assets. Generally speaking, digital screens saturate colors more deeply than printed materials do, so print-specific colors codes are used to create more visual consistency between the two formats. Some print use cases include pop up banners, swag and merchandise, stickers, billboards, and trade show booths.

- CMYK: CMYK is an acronym for Cyan, Magenta, Yellow, and Key (black). If an asset is a 'full-color' print, then CMYK is used. CMYK is usually the default for general printing and can be found in personal and commercial printers alike.
  - **Example:** C:90, M:68, Y:41, K:90
- PMS: PMS is an acronym for Pantone Matching System. Whereas CMYK colors are generated by ink values, PMS colors are pre-mixed, universal colors established by [Pantone](https://www.pantone.com/). This technique provides the most consistent color across materials. PMS is used for **spot color** prints (examples include embroidery and screen printing) and for more advanced print jobs to ensure quality and accuracy; vendors will usually specify when PMS is required. PMS colors have either a name or a code, which consists of a set of numbers followed by either a 'C' for **coated** or a 'U' for **uncoated**. Coated and uncoated refer to the paper; coated indicates gloss and uncoated refers to no coating/gloss (ie: matte paper).
  - **Example:** 433 C / 4280 U
