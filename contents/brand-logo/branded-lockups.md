---
name: Branded lockups
---

## Co-branded lockups

Co-branded lockups are created by the Brand Design team for approved GitLab partnerships, events, and campaigns. We default to the partner company’s co-branding lockup standards, but adhere to the following when such standards do not exist:

- Use the core GitLab logo.
- Ensure both logos are visually equal in size.
- Insert a gray line (Gray 03) between the logos equal to the height of the GitLab logo.
- Center both logos vertically; the company’s logo should not exceed the height of the tanuki.

Ensure proper clear space (x-height equal to the height of the letter 'a' from our wordmark) around all sides of both logos.

<figure-img label="Co-branding lockup with clear space equal to the width of the lowercase 'a' from the wordmark" src="/img/brand/co-branding.svg"></figure-img>

## Program lockups

Program lockups are used to identify GitLab’s various marketing efforts around product-related topics or bring general awareness to GitLab’s offerings; this occurs through programs, initiatives, and communities that are promoted both internally and externally. The lockup should always be accompanied by GitLab branding.

For GitLab team members looking to create a lockup for their program, please access this [self-service template](https://www.canva.com/design/DAFF2cc_ddk/T_xmiJbe67rGMBbCa-_VOg/view?utm_content=DAFF2cc_ddk&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton&mode=preview) via Canva, or request a new design using the appropriate [issue template](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/#requesting-support). The format of the lockup is as follows:

- Begin with _GitLab_ on its own line, followed by the _program name_. Text should not exceed 3 lines total or 12 characters per line.
- Use Inter Semi Bold for _GitLab_ and Inter Regular for the _program name_. Set the leading to 112% (1.1 in Canva) and tracking to 0.
- Default to Charcoal for the fill color of the entire lockup.
- Add the lockup’s logomark; this shape extends from the top of the G down to the baseline of the second line of text and features two rounded corners, with the pointed corner leading to GitLab.
- Select a white icon from the [icon library](https://drive.google.com/drive/folders/1dsRceA94H8CI0q7JAeWwEuWoNUuqdGq-?usp=sharing) that represents the program. Center it in the logomark. There should be at least 30% padding around the icon. If you need a new icon created, please submit an issue for a new design request.
- Contact the [Brand Design team](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/#contacting-the-team) for approval of the final lockup. You can also access the [design approval feature](https://www.canva.com/help/get-approval/) in Canva for review.

Ensure proper clear space (x-height equal to the height of the logomark) around all sides of the lockup. Program lockups should always be accompanied by the GitLab logo and never exceed 75% of the width of the GitLab logo on any given asset.

<figure-img label="Program lockup with clear space equal to the height of the logomark" src="/img/brand/program-lockup-clearspace.svg" width="480"></figure-img>

<figure-img label="Mockup of program lockup paired with the GitLab logo and branding" src="/img/brand/program-lockup-mockup.png" width="480"></figure-img>
